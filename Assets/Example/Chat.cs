﻿using UnityEngine;
using System;
using System.Text;
using System.Linq;
using UnityEngine.UI;

public class Chat : MonoBehaviour
{
	private WebSocket _socket;
	private InputField _messageText;
	private Button _btnSend;
	private Text _chatField;
	private string _nick;
	private string _message;
    private bool _connected;
	
	public Button BtnConnect;
	

	public void Awake()
	{
		_socket = new WebSocket(new Uri("ws://127.0.0.1:11001/Chat"));
		_socket.ChangeStateEvent += OnChangeState; 
	}

	private void OnChangeState(string obj)
	{
		var message = string.Empty;

		if (obj == "Message")
		{
            //message = _socket.RecvString();
            var data = _socket.Recv();
            if (data == null)
            {
                print("Recive empty array");
                return;
            }
            string reply = Encoding.UTF8.GetString(data);
            message = reply;
			print(message);
			_message = message;

        }
		else if (obj == "Open")
		{
            _connected = true;
            message = "Open";
			print(message);
			_message = message;
		}
		else if (obj == "Close")
		{
			message = "Close";
			print(message);
			_message = message;
		}
		else if (obj == "Error")
		{
			message = _socket.error;
			print(message);
			_message = message;
		}
	}

	private void FixedUpdate()
	{
        if(!_connected)
        {
            return;
        }

		if (_chatField == null || string.IsNullOrEmpty(_message))
		{
            if( _chatField == null)
            {
                print("_chatField is null");
            }
			return;
		}

		_chatField.text = _chatField.text + Environment.NewLine + _message;
		_message = string.Empty;
	}

	private void ShowChatUI(RectTransform loginPanel)
	{
		loginPanel.gameObject.SetActive(false);
		var chatPanel = FindObjectOfType<Canvas>().gameObject.GetComponentsInChildren<RectTransform>(true).First(o => o.name == "Chat");
		chatPanel.gameObject.SetActive(true);
		_chatField = chatPanel.GetComponentsInChildren<Text>().First(o => o.name == "ChatText");
		_messageText = chatPanel.GetComponentInChildren<InputField>();
		_btnSend = chatPanel.GetComponentInChildren<Button>();
		_btnSend.onClick.AddListener(SendMessage);
	}

	private void Start () 
	{
		BtnConnect.onClick.AddListener(Connect);
	}

	private void Connect()
	{
		BtnConnect.onClick.RemoveAllListeners();
		var loginPanel = GameObject.Find("Login").GetComponent<RectTransform>();
		var input = loginPanel.GetComponentInChildren<InputField>();
		if (input != null)
		{
			_nick = input.text;
		}
		
		_socket.Connect();
		
		print("Connected");

		ShowChatUI(loginPanel);
	}

	private void SendMessage()
	{
		var message = _messageText.text;
		if (!string.IsNullOrEmpty(message))
		{
			//_socket.SendString(message);
			_socket.Send(Encoding.UTF8.GetBytes(message));
            _messageText.text = string.Empty;
		}
	}

    #if UNITY_WEBGL && !UNITY_EDITOR
	private void OnApplicationQuit()
	{
		_socket.ChangeStateEvent -= OnChangeState;
		BtnConnect.onClick.RemoveAllListeners();
		_btnSend.onClick.RemoveAllListeners();
		_socket.Close();
	}
    #endif
}
