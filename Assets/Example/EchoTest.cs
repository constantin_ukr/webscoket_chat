﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;
using System.Text;

public class EchoTest : MonoBehaviour
{
    private WebSocket _socket;
    private InputField _messageText;
    private Button _btnSend;
    private Text _chatField;
    private RectTransform _loginPanel;
    private string _nick;
    private bool _isReady;

    public Button BtnConnect;

    private IEnumerator Start()
    {
        BtnConnect.onClick.AddListener(Auth);

        yield return StartCoroutine(Ready());

        ShowChatUI(_loginPanel);

        var url = string.Format("ws://127.0.0.1:11001/Chat?name={0}", _nick);
        print("Start. URL: " + url);
        _socket = new WebSocket(new Uri(url));
        // uncomit for EchoTestScript
        //yield return StartCoroutine(_socket.Connect());

        //_socket.SendString("Hi there");
        _socket.Send(new byte[] { 0 });
        while (true)
        {
            var data = _socket.Recv();
            if(data == null)
            {
                print("Recive empty array");
                yield return 0;
                continue;
            }
            string reply = Encoding.UTF8.GetString(data); //_socket.RecvString();
            if (reply != null)
            {
                //Debug.Log("Received: " + reply);
                _chatField.text = _chatField.text + Environment.NewLine + reply;
                //_socket.SendString("Hi there" + i++);
            }
            if (_socket.error != null)
            {
                _chatField.text = _chatField.text + Environment.NewLine + _socket.error;
                break;
            }
            yield return 0;
        }
        _socket.Close();
    }

    private void Auth()
    {
        _loginPanel = GameObject.Find("Login").GetComponent<RectTransform>();
        var input = _loginPanel.GetComponentInChildren<InputField>();
        if (input != null)
        {
            if (string.IsNullOrEmpty(input.text))
            {
                return;
            }
            _nick = input.text;
            print("Auth. Nick: " + _nick);
            BtnConnect.onClick.RemoveAllListeners();
            _isReady = true;
        }
    }

    private void ShowChatUI(RectTransform loginPanel)
    {
        loginPanel.gameObject.SetActive(false);
        var panels = FindObjectOfType<Canvas>().gameObject.GetComponentsInChildren<RectTransform>(true);
        RectTransform chatPanel = null;
        for (int i = 0; i < panels.Length; i++)
        {
            if (panels[i].name == "Chat")
            {
                chatPanel = panels[i];
                break;
            }
        }        
        chatPanel.gameObject.SetActive(true);
        var texts = chatPanel.GetComponentsInChildren<Text>();
        for (int i = 0; i < texts.Length; i++)
        {
            if (texts[i].name == "ChatText")
            {
                _chatField = texts[i];
                break;
            }
        }            
        _messageText = chatPanel.GetComponentInChildren<InputField>();
        _btnSend = chatPanel.GetComponentInChildren<Button>();
        _btnSend.onClick.AddListener(SendMessage);
    }
    
    private IEnumerator Ready()
    {
        while (!_isReady)
        {
            yield return 0;
        }
        yield break;
    }

    private void SendMessage()
    {
        _socket.SendString(_messageText.text);
        _messageText.text = string.Empty;
    }
}
